# Import the required libraries
import torch
import torch.nn as nn
import torch.optim as optim

# Create synthetic data
# x is our input, y is our output
x = torch.tensor([[1.0], [2.0], [3.0], [4.0]], requires_grad=False)
y = torch.tensor([[2.0], [4.0], [6.0], [8.0]], requires_grad=False)


# Define the model (in this case, a simple linear regression)
class LinearRegression(nn.Module):
    def __init__(self):
        super().__init__()
        self.linear = nn.Linear(1, 1)  # 1 input and 1 output

    def forward(self, x):
        return self.linear(x)


# Initialize the model, loss function, and optimizer
model = LinearRegression()
criterion = nn.MSELoss()
optimizer = optim.SGD(model.parameters(), lr=0.01)  # Using Stochastic Gradient Descent

# Train the model
for epoch in range(1000):  # number of epochs
    # Forward pass: Compute predicted y by passing x to the model
    pred_y = model(x)

    # Compute and print loss
    loss = criterion(pred_y, y)
    if (epoch % 100 == 0):
        print(f'Epoch {epoch + 1}, Loss: {loss.item()}')

    # Zero gradients, perform a backward pass, and update the weights
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

# Test the trained model
test_x = torch.tensor([[5.0]])
predicted_y = model(test_x).data[0][0]
print(f'Prediction after training for x=5: {predicted_y.item()}')
