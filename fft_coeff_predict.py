import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
import numpy as np

# Hyperparameters
N = 600
T = 1.0 / 800.0
N_fut = int(1.5 * N)
batch_size = 64
learning_rate = 0.001
epochs = 100

# Create synthetic dataset
num_samples = 1000
train_data = []
train_labels = []

for _ in range(num_samples):
    x = torch.linspace(0.0, N * T, N, dtype=torch.float32)
    y = torch.sin(50.0 * 2.0 * np.pi * x) + 0.5 * torch.sin(80.0 * 2.0 * np.pi * x)
    yf = torch.fft.fft(y)

    x_fut = torch.linspace(0.0, N_fut * T, N_fut, dtype=torch.float32)
    y_fut = torch.sin(50.0 * 2.0 * np.pi * x_fut) + 0.5 * torch.sin(80.0 * 2.0 * np.pi * x_fut)
    yf_fut = torch.fft.fft(y_fut)

    train_data.append(torch.stack([yf.real, yf.imag], dim=1))
    train_labels.append(torch.stack([yf_fut.real, yf_fut.imag], dim=1))

train_data = torch.stack(train_data)
train_labels = torch.stack(train_labels)


# Neural network model
class FFTPredictor(nn.Module):
    def __init__(self):
        super(FFTPredictor, self).__init__()
        self.fc1 = nn.Linear(N * 2, 512)
        self.fc2 = nn.Linear(512, 512)
        self.fc3 = nn.Linear(512, N_fut * 2)

    def forward(self, x):
        x = x.view(-1, N * 2)
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = self.fc3(x)
        return x.view(-1, N_fut, 2)


# Training
model = FFTPredictor()
criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

train_data = train_data.view(-1, N, 2).float()
train_labels = train_labels.view(-1, N_fut, 2).float()

for epoch in range(epochs):
    optimizer.zero_grad()
    outputs = model(train_data)
    loss = criterion(outputs, train_labels)
    loss.backward()
    optimizer.step()
    if epoch % 100 == 0:
        print(f'Epoch [{epoch + 1}/{epochs}], Loss: {loss.item():.4f}')

# Testing
x_test = torch.linspace(0.0, N * T, N, dtype=torch.float32)
y_test = torch.sin(50.0 * 2.0 * np.pi * x_test) + 0.5 * torch.sin(80.0 * 2.0 * np.pi * x_test)
yf_test_complex = torch.fft.fft(y_test)
yf_test = torch.stack([yf_test_complex.real, yf_test_complex.imag], dim=1).view(1, N, 2).float()

with torch.no_grad():
    yf_pred = model(yf_test)
    yf_pred = yf_pred.view(N_fut, 2)
    yf_pred_complex = yf_pred[:, 0] + 1j * yf_pred[:, 1]

# Perform inverse FFT to reconstruct the signal
y_pred = torch.fft.ifft(yf_pred_complex)


# Plot
plt.figure()


# Reconstructed signal
x_fut = torch.linspace(0.0, N_fut * T, N_fut, dtype=torch.float32)
plt.plot(x_fut, y_pred.real, label='Reconstructed Extended Signal')  # Consider only the real part

# Original signal
x_original = torch.linspace(0.0, N * T, N, dtype=torch.float32)
plt.plot(x_original, y_test, label='Original Signal')



plt.title('Original and Reconstructed Extended Signal')
plt.xlabel('Time [s]')
plt.ylabel('Amplitude')
plt.legend()

plt.tight_layout()
plt.show()
