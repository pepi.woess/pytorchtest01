import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
import numpy as np
import yfinance

# Check if CUDA is available and set the device
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Hyperparameters
N = 50  # Number of sample points
N_fut = int(1.5 * N)  # Length of the extended signal
learning_rate = 0.001
epochs = 100000

# Download stock data
##tickers = ['AAPL', 'MSFT', 'GOOGL', 'TSLA', 'AMZN', 'FB', 'NVDA', 'AMD', 'ADBE', 'CRM', 'INTC', 'ORCL', 'IBM', 'CSCO', 'QCOM', 'SAP', 'SQ', 'SHOP', 'UBER', 'ZM', 'SNOW', 'PLTR', 'TWLO', 'DOCU']

tickers = [
    'AAPL', 'MSFT', 'GOOGL', 'AMZN', 'FB', 'NVDA', 'AMD', 'ADBE', 'CRM',
    'INTC', 'ORCL', 'IBM', 'CSCO', 'QCOM', 'SAP', 'SQ', 'SHOP', 'UBER', 'ZM',
    'SNOW', 'PLTR', 'TWLO', 'DOCU',

    # Technology and Software
    'NOW', 'TEAM', 'OKTA', 'DDOG', 'FSLY', 'ZS', 'PANW', 'NET', 'MDB', 'WDAY',

    # E-commerce
    'BABA', 'ETSY', 'MELI', 'JD', 'W',

    # Health Care
    'JNJ', 'PFE', 'MRNA', 'ABBV', 'TDOC', 'GILD', 'VRTX', 'REGN', 'ILMN', 'DGX',

    # Finance
    'JPM', 'GS', 'MS', 'V', 'MA', 'PYPL', 'AXP', 'BLK', 'MCO', 'SCHW',

    # Consumer Goods
    'PG', 'KO', 'PEP', 'CL', 'EL', 'KMB', 'MO', 'PM', 'BTI', 'MDLZ',

    # Retail
    'WMT', 'HD', 'LOW', 'TGT', 'COST', 'DG', 'DLTR', 'BBY', 'TJX', 'ROST',

    # Telecom
    'T', 'VZ', 'TMUS', 'DISCA', 'FOXA',

    # Energy
    'XOM', 'CVX', 'COP', 'EOG', 'PXD',

    # Transportation
    'FDX', 'UPS', 'UAL', 'AAL', 'DAL',

    # Entertainment
    'DIS', 'NFLX', 'CMCSA', 'CHTR', 'ATVI',

    # Real Estate
    'SPG', 'PLD', 'PSA', 'AVB', 'EQR',

    # Utilities
    'NEE', 'DUK', 'D', 'SO', 'EXC'
]


train_data = []
train_labels = []
train_scales = []
train_scales_fut = []

for ticker in tickers:
    stock_data = yfinance.download(ticker, start='2023-01-01', end='2023-08-15')['Close'].values

    if stock_data.size < N_fut:
        print(f"Insufficient data for {ticker}, skipping...")
        continue

    for i in range(len(stock_data) - 2*N_fut):
        y = torch.tensor(stock_data[i:i + N], dtype=torch.float32)
        # Find the maximum absolute value among the real and imaginary components


        yf_raw = torch.fft.fft(y)

        maxf = torch.max(torch.abs(yf_raw.real).max(), torch.abs(yf_raw.imag).max())
        yf = yf_raw/maxf
        y_fut = torch.tensor(stock_data[i:i + N_fut], dtype=torch.float32)

        yf_fut_raw = torch.fft.fft(y_fut)
        maxf_fut = maxf #torch.max(torch.abs(yf_fut_raw.real).max(), torch.abs(yf_fut_raw.imag).max())
        yf_fut = yf_fut_raw/maxf_fut

        train_scales.append(maxf)
        train_scales_fut.append(maxf_fut)
        train_data.append(torch.stack([yf.real, yf.imag], dim=1))
        train_labels.append(torch.stack([yf_fut.real, yf_fut.imag], dim=1))

if len(train_data) == 0:
    print("No sufficient data for any ticker. Exiting.")
    exit()

# Move data to device
train_data = torch.stack(train_data).float().to(device)
train_labels = torch.stack(train_labels).float().to(device)

# Neural Network Model
class FFTPredictor(nn.Module):
    def __init__(self):
        super(FFTPredictor, self).__init__()
        self.fc1 = nn.Linear(N * 2, 512)
        self.fc2 = nn.Linear(512, 512)
        self.fc3 = nn.Linear(512, N_fut * 2)

    def forward(self, x):
        x = x.view(-1, N * 2)
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = self.fc3(x)
        return x.view(-1, N_fut, 2)

# Move model to device
model = FFTPredictor().to(device)

# Training setup
criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

# Training loop
for epoch in range(epochs):
    optimizer.zero_grad()
    outputs = model(train_data)
    loss = criterion(outputs, train_labels)
    loss.backward()
    optimizer.step()
    if epoch % 100 == 0:
        print(f'Epoch [{epoch + 1}/{epochs}], Loss: {loss.item():.9f}')

# Download and predict Tesla stock
tesla_stock = yfinance.download('IBM', start='2023-01-01', end='2023-08-31')
stock_close_recent = tesla_stock['Close'].values[-N:]
stock_data = torch.tensor(stock_close_recent, dtype=torch.float32).to(device)
yf_stock_raw = torch.fft.fft(stock_data)

yf_stock_max = torch.max(torch.abs(yf_stock_raw.real).max(), torch.abs(yf_stock_raw.imag).max())
yf_stock = yf_stock_raw/yf_stock_max

yf_stock = torch.stack([yf_stock.real, yf_stock.imag], dim=1).view(1, N, 2).float().to(device)

# Make a prediction
with torch.no_grad():
    yf_pred = model(yf_stock) * yf_stock_max
    yf_pred = yf_pred.view(N_fut, 2)
    yf_pred_complex = yf_pred[:, 0] + 1j * yf_pred[:, 1]

# Perform inverse FFT
stock_pred = torch.fft.ifft(yf_pred_complex).real.cpu().numpy()

# Plotting
plt.figure()
plt.plot(range(N), stock_close_recent, label='Original TSLA Prices')
plt.plot(range(N_fut), stock_pred, label='Predicted TSLA Prices')
plt.title('Tesla Stock Price Prediction')
plt.xlabel('Days')
plt.ylabel('Price (USD)')
plt.legend()
plt.tight_layout()
plt.show()
