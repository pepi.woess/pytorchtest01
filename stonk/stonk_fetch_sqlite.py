from alpaca.data import Adjustment
from alpaca.data.historical import StockHistoricalDataClient
from alpaca.data.requests import StockBarsRequest
from alpaca.data.timeframe import TimeFrame, TimeFrameUnit
from datetime import datetime, timedelta
import os
import pandas as pd
import sqlite3
from typing import List, Union


def create_database(db_path: str):
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS stock_data (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            timeframe TEXT,
            symbol TEXT,
            timestamp TEXT,
            adjustment TEXT,
            compatibility_id INTEGER,
            open REAL,
            high REAL,
            low REAL,
            close REAL,
            volume INTEGER,
            trade_count INTEGER,
            vwap REAL,
            last_updated TEXT,
            UNIQUE(timeframe, symbol, timestamp, adjustment, compatibility_id)
        );
    """)
    conn.commit()
    conn.close()


def fetch_and_save_stock_data_impl(
        api_key: str,
        api_secret: str,
        symbols: List[str],
        end_date: datetime,
        days_back: int,
        db_path: str,
        timeframe: TimeFrame,
        adjustment: Adjustment,
        compatibility_id: int
) -> int:
    client = StockHistoricalDataClient(api_key, api_secret)
    start_date = end_date - timedelta(days=days_back)
    request_params = StockBarsRequest(
        symbol_or_symbols=symbols,
        timeframe=timeframe,
        start=start_date,
        end=end_date,
        adjustment=adjustment
    )
    bars_all_symbols = client.get_stock_bars(request_params)
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    downloaded_rows = 0
    missing_symbols = set()  # Set to store missing symbols
    for symbol in symbols:
        try:
            bars = bars_all_symbols[symbol]
        except KeyError as e:
            error_message = str(e)
            start_index = error_message.find("No key ") + len("No key ")
            end_index = error_message.find(" was found.")
            extracted_symbol = error_message[start_index:end_index]
            # print(f"KeyError occurred. Symbol not found: {extracted_symbol}")
            missing_symbols.add(extracted_symbol)  # Add the missing symbol to the set
            continue

        downloaded_rows += len(bars)
        for bar in bars:
            cursor.execute("""
                INSERT OR REPLACE INTO stock_data (
                    timeframe, 
                    symbol, 
                    timestamp, 
                    adjustment, 
                    compatibility_id, 
                    open, 
                    high, 
                    low, 
                    close, 
                    volume, 
                    trade_count, 
                    vwap, 
                    last_updated
                ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
            """, (
                f"{timeframe.unit_value}.{timeframe.amount_value}",  # timeframe
                bar.symbol,  # symbol
                bar.timestamp,  # timestamp
                adjustment.value,  # adjustment
                compatibility_id,  # compatibility_id
                bar.open,  # open
                bar.high,  # high
                bar.low,  # low
                bar.close,  # close
                bar.volume,  # volume
                bar.trade_count,  # trade_count
                bar.vwap,  # vwap
                datetime.now()  # last_updated
            ))
    conn.commit()
    conn.close()
    if missing_symbols:
        print(f"Missing symbols: {missing_symbols}")
    return downloaded_rows


def read_stock_data_from_db(
        db_path: str,
        symbols: List[str],
        timeframe: TimeFrame,
        end_date: datetime,
        days_back: int
) -> pd.DataFrame:
    conn = sqlite3.connect(db_path)
    start_date = end_date - timedelta(days=days_back)
    query = f"""
    SELECT * FROM stock_data
    WHERE symbol IN ({','.join(['?' for _ in symbols])})
    AND timeframe = ?
    AND timestamp >= ?
    AND timestamp <= ?
    """
    df = pd.read_sql_query(
        query,
        conn,
        params=symbols + [f"{timeframe.unit_value}.{timeframe.amount_value}", start_date, end_date]
    )
    conn.close()
    return df


def summarize_changes(old_data: pd.DataFrame, new_data: pd.DataFrame) -> int:
    old_data = old_data.drop(columns=['id'], errors='ignore')
    new_data = new_data.drop(columns=['id'], errors='ignore')
    merged_data = pd.merge(old_data, new_data, on=['symbol', 'timestamp', 'timeframe', 'adjustment', 'compatibility_id'], suffixes=('_old', '_new'))

    # Check if merged_data is empty
    if merged_data.empty:
        print("No changes in the data.")
        return 0  # Return 0 as no rows have changed

    # def row_has_changed(row):
    #     old_values = list(row.filter(like='_old'))
    #     new_values = list(row.filter(like='_new'))
    #     return old_values != new_values

    def row_has_changed(row):
        old_values = row.filter(like='_old')
        new_values = row.filter(like='_new')

        # Fields to ignore during comparison
        ignored_fields = ['last_updated']

        changed_fields = []
        for old_col, new_col in zip(old_values.index, new_values.index):
            base_field_name = old_col.replace('_old', '')
            if base_field_name in ignored_fields:
                continue  # Skip this field

            if old_values[old_col] != new_values[new_col]:
                changed_fields.append(base_field_name)

        if changed_fields:
            print(f"Changed fields: {', '.join(changed_fields)}")
            return True
        else:
            return False

    rows_with_changes = merged_data.apply(row_has_changed, axis=1)
    changed_data = merged_data[rows_with_changes]
    changed_rows = 0
    if changed_data.empty:
        print("No changes in old data.")
        return changed_rows
    changed_rows = len(changed_data)
    for _, row in changed_data.iterrows():
        print(f"Data for symbol {row['symbol']} at timestamp {row['timestamp']} has changed.")
        any_field_changed = False
        for col in ['open', 'high', 'low', 'close', 'volume', 'trade_count', 'vwap']:
            old_value, new_value = row[f"{col}_old"], row[f"{col}_new"]
            if old_value != new_value:
                print(f"  {col.capitalize()}: {old_value} -> {new_value}")
                any_field_changed = True
        if not any_field_changed:
            print("  No specific fields have changed. This could be a false positive.")
    return changed_rows


def fetch_and_save_stock_data_chunked(
        api_key: str,
        api_secret: str,
        symbols: List[str],
        end_date: datetime,
        days_back: int,
        db_path: str,
        timeframe: TimeFrame,
        days_chunk: int,  # Existing parameter
        symbols_chunk: int  # New parameter
) -> None:
    total_downloaded_rows = 0
    total_changed_rows = 0
    total_new_rows = 0

    for i in range(0, days_back, days_chunk):
        chunk_end_date = end_date - timedelta(days=i)
        chunk_start_date = chunk_end_date - timedelta(days=days_chunk)

        for j in range(0, len(symbols), symbols_chunk):  # New loop to handle symbol chunks
            chunk_symbols = symbols[j:j + symbols_chunk]

            print(f"Fetching data for symbols {chunk_symbols} from {chunk_start_date} to {chunk_end_date}")

            old_data = read_stock_data_from_db(db_path, chunk_symbols, timeframe, chunk_end_date, days_chunk)
            downloaded_rows =  fetch_and_save_stock_data_impl(api_key, api_secret, chunk_symbols, chunk_end_date, days_chunk, db_path, timeframe, Adjustment.ALL, 1)
            downloaded_rows += fetch_and_save_stock_data_impl(api_key, api_secret, chunk_symbols, chunk_end_date,days_chunk, db_path, timeframe, Adjustment.RAW, 1)
            downloaded_rows += fetch_and_save_stock_data_impl(api_key, api_secret, chunk_symbols, chunk_end_date,days_chunk, db_path, timeframe, Adjustment.SPLIT, 1)
            downloaded_rows += fetch_and_save_stock_data_impl(api_key, api_secret, chunk_symbols, chunk_end_date,days_chunk, db_path, timeframe, Adjustment.DIVIDEND, 1)
            new_data = read_stock_data_from_db(db_path, chunk_symbols, timeframe, chunk_end_date, days_chunk)
            changed_rows = summarize_changes(old_data, new_data)

            new_rows = len(new_data) - len(old_data)

            # Summary for each chunk
            print(
                f"Chunk Summary: rows-downloaded-count = {downloaded_rows}, new-rows-count = {new_rows}, rows-changed-count = {changed_rows}")

            total_downloaded_rows += downloaded_rows
            total_new_rows += new_rows
            total_changed_rows += changed_rows

    print(
        f"Overall Summary: rows-downloaded-count = {total_downloaded_rows}, new-rows-count = {total_new_rows}, rows-changed-count = {total_changed_rows}")



if __name__ == "__main__":
    api_key = 'PKNR6W6Q9CQLVESNFOJY'  # Replace with your own API key
    api_secret = 'kbIkpdu5Fix6DM3sFgGvAonJa1OGF33JLmxfYElu'  # Replace with your own API secret

    nasdaq_100_symbols = [
        "AAPL", "ABNB", "ADBE", "ADI", "ADP", "ADSK", "AEP", "ALGN", "AMAT", "AMD",
        "AMGN", "AMZN", "ANSS", "ASML", "AVGO", "AZN", "BIIB", "BKNG", "BKR", "CDNS",
        "CEG", "CHTR", "CMCSA", "COST", "CPRT", "CRWD", "CSCO", "CSGP", "CSX", "CTAS",
        "CTSH", "DDOG", "DLTR", "DXCM", "EA", "EBAY", "ENPH", "EXC", "FANG", "FAST",
        "FTNT", "GEHC", "GFS", "GILD", "GOOG", "GOOGL", "HON", "IDXX", "ILMN", "INTC",
        "INTU", "ISRG", "JD", "KDP", "KHC", "KLAC", "LCID", "LRCX", "LULU", "MAR",
        "MCHP", "MDLZ", "MELI", "META", "MNST", "MRNA", "MRVL", "MSFT", "MU", "NFLX",
        "NVDA", "NXPI", "ODFL", "ON", "ORLY", "PANW", "PAYX", "PCAR", "PDD", "PEP",
        "PYPL", "QCOM", "REGN", "ROST", "SBUX", "SGEN", "SIRI", "SNPS", "TEAM", "TMUS",
        "TSLA", "TTD", "TXN", "VRSK", "VRTX", "WBA", "WBD", "WDAY", "XEL", "ZM", "ZS"
    ]

    # symbols = ['AEP', 'META', 'AMZN', 'NFLX', 'GOOGL', 'TSLA', 'AAPL']  # Updated symbols list
    symbols = nasdaq_100_symbols

    #end_date = datetime.now() - timedelta(days=1)
    end_date = datetime.strptime('2023-07-21 23:00', '%Y-%m-%d %H:%M')
    #end_date: datetime = datetime(2020, 9, 15, 0, 0, 0)
    days_back = 365*7
    # days_back = 365*10  # Increase this to a larger number
    db_path = "D:\\alpaca\\stonkdata\\stock_data.db"
    timeframe = TimeFrame(1, TimeFrameUnit.Minute)
    days_chunk = 7  # Number of days to fetch in each chunk
    symbols_chunk = 5  # Number of symbols to fetch in each chunk

    create_database(db_path)
    fetch_and_save_stock_data_chunked(api_key, api_secret, symbols, end_date, days_back, db_path, timeframe, days_chunk, symbols_chunk)
