import random
from datetime import datetime, timedelta
import sqlite3

import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset, DataLoader
from sklearn.preprocessing import StandardScaler


class StockDataset(Dataset):
    def __init__(self, db_path, item_count=10, timedelta_lookback=timedelta(days=7), timedelta_label=timedelta(days=1)):
        self.db_path = db_path
        self.timedelta_lookback = timedelta_lookback
        self.timedelta_label = timedelta_label

        self.scalers = []
        self.items = []
        for _ in range(item_count):
            scaler = StandardScaler(copy=True, with_mean=False, with_std=True)
            self.scalers.append(scaler)
            self.items.append(self._fetch_random_item(scaler))


    def _fetch_random_item(self, scaler : StandardScaler):
        # Connect to the SQLite database
        conn = sqlite3.connect(self.db_path)

        # Fetch a random symbol from stock_symbols table
        symbol_df = pd.read_sql("SELECT * FROM stock_symbols ORDER BY RANDOM() LIMIT 1", conn)
        if symbol_df.empty:
            raise ValueError("No symbols available in stock_symbols table")
        symbol = symbol_df.at[0, 'symbol']

        # Get max_timestamp and min_timestamp and convert them to datetime objects
        max_timestamp = datetime.strptime(symbol_df.at[0, 'max_timestamp'], "%Y-%m-%d %H:%M:%S+00:00")
        min_timestamp = datetime.strptime(symbol_df.at[0, 'min_timestamp'], "%Y-%m-%d %H:%M:%S+00:00")

        minutes_available = (max_timestamp - min_timestamp).total_seconds() / 60
        minutes_needed = (self.timedelta_lookback + self.timedelta_label).total_seconds() / 60

        if minutes_available > minutes_needed:
            end_timestamp = max_timestamp - timedelta(minutes=random.randint(0, (minutes_available-minutes_needed)))
        else:
            end_timestamp = max_timestamp

        start_timestamp = end_timestamp - timedelta(minutes=minutes_needed)

        # Calculate random end_timestamp within the valid range ensuring there is enough data
        #end_timestamp = max_timestamp - timedelta(days=(self.label_days + random.randint(0, (max_timestamp - min_timestamp).days - self.lookback_days - self.label_days)))
        #start_timestamp = end_timestamp - timedelta(days=self.lookback_days)

        # Fetch data for the given symbol within the calculated date range
        query = f"SELECT * FROM stock_data WHERE timeframe = 'TimeFrameUnit.Minute.1' AND symbol = '{symbol}' AND timestamp BETWEEN '{start_timestamp}' AND '{end_timestamp}' AND adjustment = 'all' AND compatibility_id = 1 ORDER BY timestamp ASC"
        data = pd.read_sql(query, conn)
        data['timestamp'] = pd.to_datetime(data['timestamp'])

        data = self._fill_missing_dataframe_rows(data, symbol, start_timestamp, end_timestamp)

        # Getting first 3 rows from df
        df_first= data.head(10)

        # Printing df_first_3
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            print(df_first)

        return data[['id', 'timestamp', 'vwap', 'volume']]


    def _fill_missing_dataframe_rows(self, df, symbol, start_timestamp, end_timestamp):
        # Set the timestamp column as index
        df.set_index('timestamp', inplace=True)

        # Reindex the DataFrame with all the minutes in the range of min and max timestamp
        all_minutes = pd.date_range(start=start_timestamp, end=end_timestamp, freq='T')
        df = df.reindex(all_minutes)

        # Create default row values (vwap is not included as it needs to be interpolated)
        default_row = {
            'id': 0,  # Assuming 0 is a suitable default value for id.
            'timeframe': 'TimeFrameUnit.Minute.1',  # Assuming this remains constant for all rows.
            'symbol': symbol,  # Assuming this remains constant for all rows.
            'adjustment': 'all',  # Assuming this remains constant for all rows.
            'compatibility_id': 1,  # Assuming this remains constant for all rows.
            'open': np.nan,  # Use NaN for price columns assuming 0 is not a suitable default value.
            'high': np.nan,
            'low': np.nan,
            'close': np.nan,
            'volume': 0,  # Assuming 0 is a suitable default value for volume.
            'trade_count': 0,  # Assuming 0 is a suitable default value for trade_count.
            'last_updated': pd.NaT  # Use NaT for last_updated as it's a datetime column.
        }

        # Fill NaN values with default values
        df.fillna(value=default_row, inplace=True)

        # Linearly interpolate the 'vwap' column
        df['vwap'].interpolate(method='linear', limit_direction='both', inplace=True)

        # Reset index to have timestamp as a column again
        df.reset_index(inplace=True)
        df.rename(columns={'index': 'timestamp'}, inplace=True)
        return df

    def __len__(self):
        return len(self.items)

    def __getitem__(self, idx):
        # Extract the sequence and the label for the given item
        item = self.items[idx]
        columns = ['vwap', 'volume']

        scaler = self.scalers[idx]
        scaler.fit(item[columns].values)

        sequence = item.iloc[:self.lookback_days * 390]
        label = item.iloc[self.lookback_days * 390:(self.lookback_days + self.label_days) * 390]

        # Normalize the sequence and label data
        sequence_data = scaler.transform(sequence[columns].values)
        label_data = scaler.transform(label[columns].values)

        return torch.tensor(sequence_data, dtype=torch.float32), torch.tensor(label_data, dtype=torch.float32)


def main():
    # Create Dataset
    db_path = "D:\\alpaca\\stonkdata\\stock_data.db"
    try:
        dataset = StockDataset(db_path, item_count=10)
        # Create DataLoader
        dataloader = DataLoader(dataset, batch_size=1, shuffle=True)

        # Example to iterate over batches
        for batch_idx, (sequence, label) in enumerate(dataloader):
            print(f"Batch: {batch_idx}, Sequence Shape: {sequence.shape}, Label Shape: {label.shape}")

    except ValueError as ve:
        print(ve)

    item0 = dataset.__getitem__(0)
    item1 = dataset.__getitem__(1)
    print("done")


if __name__ == "__main__":
    main()
