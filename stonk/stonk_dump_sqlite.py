import sqlite3

def create_connection(db_file):
    """Create a database connection to the SQLite database specified by db_file."""
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except sqlite3.Error as e:
        print(e)
    return conn

def dump_few_rows(conn, limit=5):
    """Dump a few rows from the stock_data table."""
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM stock_data LIMIT {limit}")
    rows = cur.fetchall()
    for row in rows:
        print(row)

if __name__ == '__main__':
    database = "D:\\alpaca\\stonkdata\\stock_data.db" # Replace with the path to your SQLite database

    # Create a database connection
    conn = create_connection(database)

    if conn:
        # Dump a few rows from the table
        print("Dumping a few rows from stock_data table:")
        dump_few_rows(conn)

        # Close the connection
        conn.close()
    else:
        print("Error! Cannot create the database connection.")
