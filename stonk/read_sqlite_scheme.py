import sqlite3
import re


def read_sqlite_schema(database_path):
    conn = sqlite3.connect(database_path)
    cursor = conn.cursor()

    # Fetch and print table information
    cursor.execute("SELECT name, sql FROM sqlite_master WHERE type='table';")
    tables = cursor.fetchall()

    for table in tables:
        table_name, create_statement = table
        print(f"Table: {table_name}")

        # Print the CREATE TABLE statement
        print(f"  CREATE TABLE Statement: {create_statement}")

        cursor.execute(f"PRAGMA table_info({table_name});")
        columns = cursor.fetchall()

        # Each column tuple has the structure: (cid, name, type, notnull, dflt_value, pk)
        for cid, name, type, notnull, dflt_value, pk in columns:
            column_info = f"  Column: {name} ({type})"
            if pk:  # Check if the column is a primary key
                column_info += " [PRIMARY KEY]"
            if notnull:  # Check if the column is NOT NULL
                column_info += " [NOT NULL]"
            if dflt_value is not None:  # Check if the column has a default value
                column_info += f" [DEFAULT: {dflt_value}]"

            # Check if the column is AUTOINCREMENT
            if re.search(rf"{name}\s+INTEGER\s+PRIMARY\s+KEY\s+AUTOINCREMENT", create_statement, re.IGNORECASE):
                column_info += " [AUTOINCREMENT]"

            print(column_info)

        # Fetch and print unique constraints
        cursor.execute(f"PRAGMA index_list({table_name});")
        indexes = cursor.fetchall()

        # Each index tuple has the structure: (seq, name, unique, origin, partial)
        for seq, name, unique, origin, partial in indexes:
            if unique:  # Check if the index is unique
                cursor.execute(f"PRAGMA index_info({name});")
                unique_columns = [col[2] for col in cursor.fetchall()]
                print(f"  UNIQUE({', '.join(unique_columns)})")

    # Fetch and print index information
    cursor.execute("SELECT name, tbl_name FROM sqlite_master WHERE type='index';")
    indexes = cursor.fetchall()

    for index in indexes:
        index_name, table_name = index
        print(f"Index: {index_name} (Table: {table_name})")

        cursor.execute(f"PRAGMA index_info({index_name});")
        index_columns = cursor.fetchall()

        # Each index column tuple has the structure: (seqno, cid, name)
        for seqno, cid, name in index_columns:
            print(f"  Indexed Column: {name}")

    conn.close()

def remove_redundant_index(db_path: str):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()

    # Drop the redundant index
    cursor.execute("DROP INDEX IF EXISTS idx_timeframe_symbol_timestamp;")

    # Commit the changes and close the connection
    conn.commit()
    conn.close()


if __name__ == "__main__":
    db_path = "D:\\alpaca\\stonkdata\\stock_data.db"

    # Remove the redundant index
    # remove_redundant_index(db_path)

    # Read and print the SQLite schema
    read_sqlite_schema(db_path)