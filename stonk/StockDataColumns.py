from enum import Enum

class StockDataColumns(Enum):
    id = 0
    timeframe = 1
    symbol = 2
    timestamp = 3
    adjustment = 4
    compatibility_id = 5
    open = 6
    high = 7
    low = 8
    close = 9
    volume = 10
    trade_count = 11
    vwap = 12
    last_updated = 13

