from alpaca.data.timeframe import TimeFrame, TimeFrameUnit
from datetime import datetime, timedelta
import pandas as pd
from typing import List, Union, Optional
import sqlite3
import pytz
import matplotlib.pyplot as plt
from mplfinance import make_addplot, plot


def read_stock_data_from_db(
        db_path: str,
        symbols: Union[str, List[str]],
        start_date: Optional[datetime] = None,
        end_date: Optional[datetime] = None,
        timeframe: Optional[TimeFrame] = None
) -> pd.DataFrame:
    # Connect to SQLite database
    conn = sqlite3.connect(db_path)

    # Prepare SQL query
    query = "SELECT * FROM stock_data WHERE "
    query_conditions = []

    if isinstance(symbols, str):
        query_conditions.append(f"symbol = '{symbols}'")
    else:
        query_conditions.append(f"symbol IN ({','.join(['?' for _ in symbols])})")

    if start_date is not None:
        query_conditions.append(f"timestamp >= '{start_date.isoformat(sep=' ')}'")

    if end_date is not None:
        query_conditions.append(f"timestamp <= '{end_date.isoformat(sep=' ')}'")

    if timeframe is not None:
        query_conditions.append(f"timeframe = '{timeframe.unit_value}.{timeframe.amount_value}'")

    query += " AND ".join(query_conditions)

    # Execute SQL query and load into DataFrame
    if isinstance(symbols, str):
        df = pd.read_sql_query(query, conn)
    else:
        df = pd.read_sql_query(query, conn, params=symbols)

    conn.close()

    # Convert the 'timestamp' column to datetime type
    df['timestamp'] = pd.to_datetime(df['timestamp'])

    return df

def plot_stock_data(df: pd.DataFrame, symbol: str):
    # Filter data for the specific symbol
    df_filtered = df[df['symbol'] == symbol]

    # Sort DataFrame by timestamp
    df_filtered = df_filtered.sort_values('timestamp')

    # Set the timestamp as the index
    df_filtered.set_index('timestamp', inplace=True)

    # Rename columns to fit what mplfinance expects
    df_filtered.rename(columns={
        'open': 'Open',
        'high': 'High',
        'low': 'Low',
        'close': 'Close',
        'volume': 'Volume'
    }, inplace=True)

    # Plotting

    plot(df_filtered, type='candle', style='charles', title=f"{symbol} Stock Price", ylabel='Price (USD)', volume=True)

def plot_stock_data2(df: pd.DataFrame, symbol: str):
    # Filter data for the specific symbol
    df_filtered = df[df['symbol'] == symbol]

    # Sort DataFrame by timestamp
    df_filtered = df_filtered.sort_values('timestamp')

    # Set the timestamp as the index
    df_filtered.set_index('timestamp', inplace=True)

    # Rename columns to fit what mplfinance expects
    df_filtered.rename(columns={
        'open': 'Open',
        'high': 'High',
        'low': 'Low',
        'close': 'Close',
        'volume': 'Volume'
    }, inplace=True)

    # Calculate a simple moving average for demonstration
    df_filtered['SMA_20'] = df_filtered['Close'].rolling(window=20).mean()

    # Create an addplot object for the moving average
    ap2 = make_addplot(df_filtered['SMA_20'], panel=0, color='r', secondary_y=False)
    ap = make_addplot(df_filtered['vwap'], panel=0, color='b', secondary_y=False)

    # Plotting
    plot(df_filtered, type='candle', style='charles', title=f"{symbol} Stock Price", ylabel='Price (USD)', volume=True, addplot=[ap, ap2])

def plot_multiple_stock_data(df: pd.DataFrame, symbols: list):
    plt.figure(figsize=(14, 8))

    for symbol in symbols:
        # Filter data for the specific symbol
        df_filtered = df[df['symbol'] == symbol]

        # Sort DataFrame by timestamp
        df_filtered = df_filtered.sort_values('timestamp')

        # Set the timestamp as the index
        df_filtered.set_index('timestamp', inplace=True)

        # Plotting
        plt.plot(df_filtered.index, df_filtered['close'], label=f"{symbol} Close Price")

    plt.title("Stock Prices")
    plt.xlabel("Timestamp")
    plt.ylabel("Price (USD)")
    plt.legend()
    plt.show()


def plot_multiple_stock_data2(df: pd.DataFrame, symbols: list):
    # Create a list to hold additional plots
    add_plots = []

    # Loop through the list of symbols to create additional plots
    for i, symbol in enumerate(symbols[1:]):
        df_filtered = df[df['symbol'] == symbol]
        df_filtered = df_filtered.sort_values('timestamp')
        df_filtered.set_index('timestamp', inplace=True)
        df_filtered.rename(columns={
            'open': 'Open',
            'high': 'High',
            'low': 'Low',
            'close': 'Close',
            'volume': 'Volume'
        }, inplace=True)

        add_plots.append(make_addplot(df_filtered['Close'], panel=i + 1, ylabel=f"{symbol} Price"))

    # Prepare the main plot for the first symbol
    df_filtered = df[df['symbol'] == symbols[0]]
    df_filtered = df_filtered.sort_values('timestamp')
    df_filtered.set_index('timestamp', inplace=True)
    df_filtered.rename(columns={
        'open': 'Open',
        'high': 'High',
        'low': 'Low',
        'close': 'Close',
        'volume': 'Volume'
    }, inplace=True)

    # Create the main plot
    plot(df_filtered, type='line', style='charles', title=f"Stock Prices", ylabel=f"{symbols[0]} Price (USD)",
         addplot=add_plots)

if __name__ == "__main__":
    # Example usage
    db_path = "D:\\alpaca\\stonkdata\\stock_data.db"
    symbols = ['TSLA', 'AAPL']

    end_date = pytz.utc.localize(datetime.strptime('2023-09-03 18:00', '%Y-%m-%d %H:%M'))
    start_date = end_date - timedelta(days=30)

    timeframe = TimeFrame.Minute  # 1 Minute timeframe

    df = read_stock_data_from_db(db_path, symbols, start_date, end_date, timeframe)

    # Plot stock data for TSLA
    plot_stock_data2(df, 'TSLA')

    #plot_multiple_stock_data(df, symbols)


    #
    # # Plot stock data for AAPL
    # plot_stock_data(df, 'AAPL')

