import os
import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta
from typing import Union
from alpaca.data.timeframe import TimeFrame, TimeFrameUnit  # Assuming these are imported or defined elsewhere

# Assuming TimeFrame and TimeFrameUnit are defined elsewhere or imported
# from alpaca.data.timeframe import TimeFrame, TimeFrameUnit

def plot_stock_data(folder_path: str, symbol: str, timeframe: TimeFrame, end_date: pd.Timestamp, days_back: int):
    """
    Reads stock data from a CSV file and plots it.

    Parameters:
        folder_path (str): The folder where the stock data CSV files are saved.
        symbol (str): The stock symbol to plot.
        timeframe (TimeFrame): The timeframe for the data (e.g., 1 minute, 1 day).
        end_date (pd.Timestamp): The end date for the data to be plotted.
        days_back (int): The number of days back from the end_date to consider for plotting.

    Returns:
        None
    """
    # Construct the file name based on the given parameters
    file_name = f"{symbol}_historical_data_{timeframe.unit_value}.{timeframe.amount_value}.csv"
    file_path = os.path.join(folder_path, file_name)

    if os.path.exists(file_path):
        # Calculate the number of rows to skip and the number of rows to read
        start_date = end_date - pd.Timedelta(days=days_back)

        # Read only the 'timestamp' column for efficiency
        df_timestamp = pd.read_csv(file_path, usecols=['timestamp'], parse_dates=['timestamp'])

        # Make the DataFrame's datetime objects timezone-naive
        df_timestamp['timestamp'] = df_timestamp['timestamp'].dt.tz_convert(None)

        start_idx = df_timestamp[df_timestamp['timestamp'] >= start_date].index.min()
        end_idx = df_timestamp[df_timestamp['timestamp'] <= end_date].index.max()

        if pd.isna(start_idx) or pd.isna(end_idx):
            print("No data available for the specified date range.")
            return

        nrows = end_idx - start_idx + 1
        skiprows = range(1, start_idx)  # Skip the header row and rows up to start_idx

        # Read only the required rows
        df = pd.read_csv(file_path, skiprows=skiprows, nrows=nrows, parse_dates=['timestamp'])

        # Plotting
        plt.figure(figsize=(14, 7))

        plt.subplot(2, 1, 1)
        plt.title(f'{symbol} Stock Data')
        plt.plot(df['timestamp'], df['close'], label='Close Price')
        plt.plot(df['timestamp'], df['open'], label='Open Price')
        plt.xlabel('Timestamp')
        plt.ylabel('Price')
        plt.legend()

        plt.subplot(2, 1, 2)
        plt.bar(df['timestamp'], df['volume'], width=0.0001)
        plt.xlabel('Timestamp')
        plt.ylabel('Volume')

        plt.tight_layout()
        plt.show()
    else:
        print(f"File {file_path} does not exist.")


if __name__ == "__main__":
    folder_path = "D:\\alpaca\\stonkdata"  # Replace with your folder path
    symbol = "TSLA"  # Replace with the stock symbol you are interested in
    timeframe = TimeFrame(1, TimeFrameUnit.Minute)  # Replace with the timeframe you are interested in
    end_date = pd.Timestamp('2023-09-01')  # This is timezone-naive
    days_back = 3  # Replace with the number of days back you are interested in

    plot_stock_data(folder_path, symbol, timeframe, end_date, days_back)
