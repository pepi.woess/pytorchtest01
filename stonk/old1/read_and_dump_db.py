import sqlite3

def read_and_dump_db(db_path):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()

    # Get the list of all tables in the database
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = cursor.fetchall()

    # Loop through each table and print a few rows
    for table in tables:
        table_name = table[0]
        print(f"Table: {table_name}")

        # Get column names
        cursor.execute(f"PRAGMA table_info({table_name});")
        columns = cursor.fetchall()
        column_names = [column[1] for column in columns]
        print("Columns:", column_names)

        # Fetch and print the row count for the table
        cursor.execute(f"SELECT COUNT(*) FROM {table_name};")
        row_count = cursor.fetchone()[0]
        print(f"Row Count: {row_count}")

        # Fetch and print the first 5 rows from the table
        print("First 5 rows:")
        cursor.execute(f"SELECT * FROM {table_name} LIMIT 5;")
        rows = cursor.fetchall()
        for row in rows:
            print(row)

        # Fetch and print the last 5 rows from the table
        print("Last 5 rows:")
        cursor.execute(f"SELECT * FROM {table_name} ORDER BY ROWID DESC LIMIT 5;")
        rows = cursor.fetchall()
        for row in reversed(rows):
            print(row)

        print("="*40)

if __name__ == "__main__":
    db_path = "D:\\alpaca\\stonkdata\\stock_data.db"
    read_and_dump_db(db_path)
