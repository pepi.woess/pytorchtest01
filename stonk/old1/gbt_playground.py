import pandas as pd

# Your list of dictionaries
data = [
    {
        'close': 258.08,
        'high': 261.18,
        'low': 255.05,
        'open': 255.98,
        'symbol': 'TSLA',
        'timestamp': pd.Timestamp('2023-08-31 04:00:00', tz='UTC'),
        'trade_count': 1066702.0,
        'volume': 108861789.0,
        'vwap': 258.03474
    },
    {
        'close': 245.01,
        'high': 259.0794,
        'low': 242.01,
        'open': 257.26,
        'symbol': 'TSLA',
        'timestamp': pd.Timestamp('2023-09-01 04:00:00', tz='UTC'),
        'trade_count': 1528802.0,
        'volume': 132539843.0,
        'vwap': 248.275783
    }
]

# Create a Pandas DataFrame
df = pd.DataFrame(data)

# Show the DataFrame
print(df)
