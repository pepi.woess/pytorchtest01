from alpaca.data.historical import StockHistoricalDataClient
from alpaca.data.requests import StockBarsRequest
from alpaca.data.timeframe import TimeFrame, TimeFrameUnit
from datetime import datetime, timedelta
import os
import pandas as pd
from typing import List, Union

def fetch_and_save_stock_data(
    api_key: str,
    api_secret: str,
    symbols: List[str],
    end_date: datetime,
    days_back: int,
    save_folder: str,
    timeframe: TimeFrame
) -> None:
    """
    Fetches and saves historical stock data.

    Parameters:
        api_key (str): The API key for the Alpaca service.
        api_secret (str): The API secret for the Alpaca service.
        symbols (List[str]): List of stock symbols to fetch data for.
        end_date (datetime): The end date for the historical data.
        days_back (int): Number of days back from the end_date to fetch data.
        save_folder (str): The folder where to save the fetched data.
        timeframe (TimeFrame): The timeframe for the data (e.g., 1 minute, 1 day).

    Returns:
        None
    """

    # Initialize the Alpaca Stock Historical Data Client
    client = StockHistoricalDataClient(api_key, api_secret)

    # Define the time range
    start_date = end_date - timedelta(days=days_back)

    # Prepare the request parameters
    request_params = StockBarsRequest(
        symbol_or_symbols=symbols,
        timeframe=timeframe,
        start=start_date,
        end=end_date
    )

    bars_all_symbols = client.get_stock_bars(request_params)

    # Create the folder if it doesn't exist
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)

    for symbol in symbols:
        bars = bars_all_symbols[symbol]
        processed_bars = []
        for bar in bars:
            processed_bar = {
                'symbol': bar.symbol,
                'timestamp': bar.timestamp,
                'open': bar.open,
                'high': bar.high,
                'low': bar.low,
                'close': bar.close,
                'volume': bar.volume,
                'trade_count': bar.trade_count,
                'vwap': bar.vwap
            }
            processed_bars.append(processed_bar)

        new_df = pd.DataFrame(processed_bars)
        new_df['last_updated'] = datetime.now()

        csv_file = os.path.join(save_folder, f"{symbol}_historical_data_{timeframe.unit_value}.{timeframe.amount_value}.csv")

        if os.path.exists(csv_file):
            existing_df = pd.read_csv(csv_file)
            merged_df = merge_dataframes(existing_df, new_df)
        else:
            merged_df = new_df

        merged_df.to_csv(csv_file, index=False)
        print(f"Downloaded historical data for {symbol} and saved to {csv_file}")

def merge_dataframes(existing_df, new_df):
    existing_df['timestamp'] = pd.to_datetime(existing_df['timestamp'])
    new_df['timestamp'] = pd.to_datetime(new_df['timestamp'])
    merged_df = pd.concat([existing_df, new_df]).reset_index(drop=True)
    merged_df.sort_values(by='timestamp', inplace=True)
    merged_df.drop_duplicates(subset='timestamp', inplace=True)
    merged_df.reset_index(drop=True, inplace=True)
    return merged_df

if __name__ == "__main__":
    api_key = 'PKNR6W6Q9CQLVESNFOJY'
    api_secret = 'kbIkpdu5Fix6DM3sFgGvAonJa1OGF33JLmxfYElu'
    symbols = ['TSLA']
    #end_date = datetime.now() - timedelta(days=1)
    end_date = datetime.strptime('2023-09-01 18:00', '%Y-%m-%d %H:%M')

    days_back = 1
    save_folder = "D:\\alpaca\\stonkdata"
    timeframe = TimeFrame(1, TimeFrameUnit.Minute)  # 1 Day timeframe

    fetch_and_save_stock_data(api_key, api_secret, symbols, end_date, days_back, save_folder, timeframe)
