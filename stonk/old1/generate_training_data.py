import numpy as np
import pandas as pd
import sqlite3
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset, DataLoader
import random
from datetime import timedelta
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt  # Import matplotlib for plotting

# Load a random 8-day timespan from the database
def load_random_timespan(conn, days=8):
    symbols = pd.read_sql_query("SELECT DISTINCT symbol FROM stock_data WHERE timeframe = 'TimeFrameUnit.Minute.1'", conn)['symbol'].tolist()
    random_symbol = random.choice(symbols)

    min_time = pd.read_sql_query(
        f"SELECT MIN(timestamp) FROM stock_data WHERE timeframe = 'TimeFrameUnit.Minute.1' AND symbol = '{random_symbol}'",
        conn).values[0][0]
    max_time = pd.read_sql_query(
        f"SELECT MAX(timestamp) FROM stock_data WHERE timeframe = 'TimeFrameUnit.Minute.1' AND symbol = '{random_symbol}'",
        conn).values[0][0]

    min_time = pd.Timestamp(min_time)
    max_time = pd.Timestamp(max_time)

    random_start_time = pd.Timestamp(random.choice(pd.date_range(min_time, max_time - pd.DateOffset(days=days))))
    random_end_time = random_start_time + pd.DateOffset(days=days)

    query = f"SELECT timestamp, volume, vwap FROM stock_data WHERE timestamp >= '{random_start_time}' AND timestamp <= '{random_end_time}' AND timeframe = 'TimeFrameUnit.Minute.1' AND symbol = '{random_symbol}' ORDER BY timestamp"
    df_sample = pd.read_sql_query(query, conn)

    df_sample['timestamp'] = pd.to_datetime(df_sample['timestamp'])
    df_sample.set_index('timestamp', inplace=True)
    df_sample = df_sample.resample('T').asfreq().fillna(0)

    scaler = MinMaxScaler()
    df_sample[['volume', 'vwap']] = scaler.fit_transform(df_sample[['volume', 'vwap']])

    stock_symbol = random_symbol
    timestamps = df_sample.index.tolist()

    scale_factors = {
        'volume': {
            'min': scaler.data_min_[0],
            'max': scaler.data_max_[0]
        },
        'vwap': {
            'min': scaler.data_min_[1],
            'max': scaler.data_max_[1]
        }
    }

    if len(df_sample) < days * 24 * 60:
        return None, None, None, None, None

    sample = df_sample.iloc[:-1]
    label = df_sample.iloc[-1]

    return sample.values, label.values, stock_symbol, timestamps, scale_factors

# Collect multiple random timespans
def collect_timespans(conn, num_samples, days=8):
    samples = []
    labels = []
    symbols = []
    all_timestamps = []
    all_scale_factors = []
    expected_length = days * 24 * 60

    for _ in range(num_samples):
        sample, label, stock_symbol, timestamps, scale_factors = load_random_timespan(conn, days)
        if sample is not None:
            if len(sample) < expected_length:
                padding = np.zeros((expected_length - len(sample), sample.shape[1]))
                sample = np.vstack([sample, padding])

            samples.append(sample)
            labels.append(label)
            symbols.append(stock_symbol)
            all_timestamps.append(timestamps)
            all_scale_factors.append(scale_factors)

    samples = np.array(samples)
    labels = np.array(labels)

    return torch.tensor(samples, dtype=torch.float32), torch.tensor(labels, dtype=torch.float32), symbols, all_timestamps, all_scale_factors

# Define the neural network model
class StockNN(nn.Module):
    def __init__(self):
        super(StockNN, self).__init__()
        self.lstm = nn.LSTM(input_size=2, hidden_size=64, num_layers=1, batch_first=True)
        self.fc = nn.Linear(64, 2)

    def forward(self, x):
        x, _ = self.lstm(x)
        x = self.fc(x[:, -1, :])
        return x

# Train the model
def train_model(samples_tensor, labels_tensor):
    dataset = TensorDataset(samples_tensor, labels_tensor)
    dataloader = DataLoader(dataset, batch_size=32, shuffle=True)

    model = StockNN()
    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    for epoch in range(100):
        for sample, label in dataloader:
            optimizer.zero_grad()
            outputs = model(sample)
            loss = criterion(outputs, label)
            loss.backward()
            optimizer.step()

    return model

if __name__ == "__main__":
    db_path = "D:\\alpaca\\stonkdata\\stock_data.db"
    conn = sqlite3.connect(db_path)
    num_samples = 10

    samples_tensor, labels_tensor, symbols, all_timestamps, all_scale_factors = collect_timespans(conn, num_samples)

    for i in range(2):
        sample = samples_tensor[i].numpy()
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.title(f"Sample {i+1}")
        plt.plot(sample[:, 0], label="Volume")
        plt.legend()

        plt.subplot(2, 1, 2)
        plt.plot(sample[:, 1], label="VWAP")
        plt.legend()

        plt.show()

    model = train_model(samples_tensor, labels_tensor)
    # print("Model training complete.")
    # print(f"Stock Symbols: {symbols}")
    # print(f"Timestamps: {all_timestamps}")
    # print(f"Scale Factors: {all_scale_factors}")
