import sqlite3
from datetime import datetime
from typing import List, Tuple, Any
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from alpaca.data import Adjustment
from StockDataColumns import StockDataColumns


def read_stock_data(database_path: str, symbol: str, timeframe: str, start_datetime: datetime, end_datetime: datetime, adjustment: str, compatibility_id: int) -> \
List[Tuple[Any, ...]]:
    # Convert datetime objects to string format for SQL query
    start_str = start_datetime.strftime('%Y-%m-%d %H:%M:%S+00:00')
    end_str = end_datetime.strftime('%Y-%m-%d %H:%M:%S+00:00')

    # Connect to the SQLite database
    conn = sqlite3.connect(database_path)
    cursor = conn.cursor()

    # SQL query to fetch data
    query = '''SELECT * FROM stock_data 
               WHERE timeframe = ? AND symbol = ? 
               AND timestamp >= ? AND timestamp <= ? 
               AND adjustment = ? AND compatibility_id = ? 
               ORDER BY timestamp ASC'''

    cursor.execute(query, (timeframe, symbol, start_str, end_str, adjustment, compatibility_id))

    # Fetch all rows that match the criteria
    rows = cursor.fetchall()

    # Close the database connection
    conn.close()

    return rows


def plot_stock_data(stock_data_list: List[List[Tuple[Any, ...]]], labels: List[str]) -> None:
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 10), sharex=True)

    # Function to format x-coordinate in the status bar
    def format_coord_ax1(x, y):
        return f'ax1 Time={mdates.num2date(x).strftime("%Y-%m-%d %H:%M:%S")}, Value={y:.2f}'

    def format_coord_ax2(x, y):
        return f'ax2 Time={mdates.num2date(x).strftime("%Y-%m-%d %H:%M:%S")}, Value={y:.2f}'

    ax1.format_coord = format_coord_ax1
    ax2.format_coord = format_coord_ax2

    for stock_data, label in zip(stock_data_list, labels):
        timestamps = [mdates.date2num(datetime.fromisoformat(row[StockDataColumns.timestamp.value])) for row in stock_data]
        vwap = [row[StockDataColumns.vwap.value] for row in stock_data]
        volume = [row[StockDataColumns.volume.value] for row in stock_data]

        # Plot VWAP
        ax1.plot(timestamps, vwap, linestyle='-', label=label)
        ax1.set_ylabel('VWAP')
        ax1.grid(True)
        ax1.set_title('Stock Data - VWAP')

        # Plot Volume
        ax2.plot(timestamps, volume, linestyle='-', label=label)
        ax2.set_xlabel('Timestamp')
        ax2.set_ylabel('Volume')
        ax2.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
        ax2.grid(True)
        ax2.set_title('Stock Data - Volume')

    ax1.legend()
    ax2.legend()

    fig.tight_layout()
    plt.show()


# Example usage
if __name__ == "__main__":
    database_path: str = "D:\\alpaca\\stonkdata\\stock_data.db"
    symbol1: str = 'TSLA'
    symbol2: str = 'TSLA'
    timeframe: str = 'TimeFrameUnit.Minute.1'
    start_datetime: datetime = datetime(2020, 7, 15, 0, 0, 0)
    end_datetime: datetime = datetime(2020, 9, 15, 0, 0, 0)

    result1: List[Tuple[Any, ...]] = read_stock_data(database_path, symbol1, timeframe, start_datetime, end_datetime, Adjustment.RAW.value, 1)
    result2: List[Tuple[Any, ...]] = read_stock_data(database_path, symbol2, timeframe, start_datetime, end_datetime, Adjustment.ALL.value, 1)

    # Plot the data
    plot_stock_data([result1, result2], [symbol1 + " RAW", symbol2 + " ALL"])
