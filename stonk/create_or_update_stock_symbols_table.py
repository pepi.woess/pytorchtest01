import sqlite3
from datetime import datetime


def create_or_update_stock_symbols_table(db_path):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()

    # Try to create the stock_symbols table if it doesn’t exist
    cur.execute("""
        CREATE TABLE IF NOT EXISTS stock_symbols (
            symbol TEXT PRIMARY KEY, 
            min_timestamp TEXT,
            max_timestamp TEXT,
            last_updated TEXT)
    """)

    # Fetch unique symbols along with their min and max timestamps from the stock_data table
    cur.execute("""
        SELECT symbol, 
               MIN(timestamp) as min_timestamp, 
               MAX(timestamp) as max_timestamp 
        FROM stock_data
        GROUP BY symbol
    """)
    symbols = cur.fetchall()

    for symbol in symbols:
        # For each unique symbol, try to insert it into the stock_symbols table.
        # If the symbol already exists, update the min, max timestamps and last_updated field.
        cur.execute("""
            INSERT INTO stock_symbols (symbol, min_timestamp, max_timestamp, last_updated) 
            VALUES (?, ?, ?, ?)
            ON CONFLICT(symbol) DO UPDATE SET 
                min_timestamp=excluded.min_timestamp, 
                max_timestamp=excluded.max_timestamp,
                last_updated=excluded.last_updated
        """, (symbol[0], symbol[1], symbol[2], datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')))

    # Commit changes and close the connection
    conn.commit()
    conn.close()


def dump_stock_symbols_table(db_path):
    # Connect to the SQLite database
    conn = sqlite3.connect(db_path)
    cur = conn.cursor()

    # Execute a SELECT statement to fetch all rows from stock_symbols table
    cur.execute("SELECT * FROM stock_symbols")

    # Fetch all rows
    rows = cur.fetchall()

    # Print column names
    print("symbol | min_timestamp | max_timestamp | last_updated")
    print("=" * 80)

    # Print each row
    for row in rows:
        print(f"{row[0]} | {row[1]} | {row[2]} | {row[3]}")

    # Close the connection
    conn.close()


if __name__ == "__main__":
    db_path = "D:\\alpaca\\stonkdata\\stock_data.db"
    #create_or_update_stock_symbols_table(db_path)
    dump_stock_symbols_table(db_path)
