import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import datasets, transforms
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt

# Function to compute the 2D FFT and return the normalized magnitude
def fft_2d(image):
    fft_result = torch.fft.fftn(image)
    magnitude = torch.sqrt(fft_result.real ** 2 + fft_result.imag ** 2)
    normalized_magnitude = (magnitude - torch.min(magnitude)) / (torch.max(magnitude) - torch.min(magnitude))
    return normalized_magnitude

# Define a modified feed-forward neural network with LeakyReLU
class ModifiedNN(nn.Module):
    def __init__(self):
        super(ModifiedNN, self).__init__()
        self.fc1 = nn.Linear(28 * 28 * 2, 128)
        self.fc2 = nn.Linear(128, 64)
        self.fc3 = nn.Linear(64, 10)
        self.leaky_relu = nn.LeakyReLU(0.01)

    def forward(self, x, fft_x):
        x_combined = torch.cat((x.view(-1, 28 * 28), fft_x.view(-1, 28 * 28)), dim=1)
        x = self.leaky_relu(self.fc1(x_combined))
        x = self.leaky_relu(self.fc2(x))
        x = self.fc3(x)
        return x

# Initialize the model, loss function, and optimizer
model = ModifiedNN()
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=0.01)

# Load MNIST data
transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))])
train_data = datasets.MNIST(root='./data', train=True, download=True, transform=transform)
train_loader = DataLoader(train_data, batch_size=32, shuffle=True)

# To store loss values for plotting
loss_values = []

# To store accuracy values for plotting
accuracy_values = []

# Train the model
for epoch in range(2):
    for i, (data, labels) in enumerate(train_loader):
        fft_data = fft_2d(data.squeeze(1))

        optimizer.zero_grad()
        output = model(data.squeeze(1), fft_data)
        loss = criterion(output, labels)
        loss.backward()
        optimizer.step()

        if i % 500 == 0:
            print(f'Epoch [{epoch + 1}/2], Step [{i + 1}/{len(train_loader)}], Loss: {loss.item():.4f}')
            loss_values.append(loss.item())

    # Test the model for each epoch
    test_data = datasets.MNIST(root='./data', train=False, download=True, transform=transform)
    test_loader = DataLoader(test_data, batch_size=32, shuffle=False)
    correct = 0
    total = 0
    with torch.no_grad():
        for data, labels in test_loader:
            fft_data = fft_2d(data.squeeze(1))
            output = model(data.squeeze(1), fft_data)
            _, predicted = torch.max(output.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
        accuracy = 100 * correct / total
        accuracy_values.append(accuracy)
        print(f'Accuracy after epoch {epoch + 1}: {accuracy:.2f}%')

# Plotting Loss
plt.figure()
plt.plot(loss_values)
plt.title('Loss over iterations')
plt.xlabel('Iterations')
plt.ylabel('Loss')
plt.show()

# Plotting Accuracy
plt.figure()
plt.plot(accuracy_values)
plt.title('Accuracy over epochs')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.show()

# Visualizing some test data along with FFT, predicted, and true labels
data, labels = next(iter(test_loader))
fft_data = fft_2d(data.squeeze(1))
output = model(data.squeeze(1), fft_data)
_, predicted = torch.max(output.data, 1)

plt.figure()
for i in range(6):
    plt.subplot(3, 6, i + 1)
    plt.imshow(data[i][0], cmap='gray')
    plt.title(f'True: {labels[i]}')
    plt.axis('off')

    plt.subplot(3, 6, i + 7)
    plt.imshow(fft_data[i], cmap='gray')
    plt.title(f'FFT')
    plt.axis('off')

    plt.subplot(3, 6, i + 13)
    plt.imshow(data[i][0], cmap='gray')
    plt.title(f'Pred: {predicted[i]}')
    plt.axis('off')

plt.tight_layout()
plt.show()
