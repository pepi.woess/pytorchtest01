import torch
import matplotlib.pyplot as plt
import numpy as np

# Create a 1D time signal
N = 600  # Number of samples
T = 1.0 / 800.0  # Sample spacing
x = torch.linspace(0.0, N*T, N, dtype=torch.float64)
y = torch.sin(50.0 * 2.0 * np.pi * x) + 0.5 * torch.sin(80.0 * 2.0 * np.pi * x)

# Perform FFT to obtain frequency components
yf = torch.fft.fft(y)
xf = torch.fft.fftfreq(N, T)

# Generate a new time array that extends into the future by 50%
N_fut = int(1.5 * N)
x_fut = torch.linspace(0.0, N_fut * T, N_fut, dtype=torch.float64)

# Initialize an empty tensor for the future signal
y_fut = torch.zeros(N_fut, dtype=torch.float64)

# Use the main frequency components to predict the future signal
# (Note: This is a simplistic example and assumes that the signal is entirely periodic)
for i in range(N):
    coef = yf[i]
    freq = xf[i]
    y_fut += coef.real * torch.cos(2.0 * np.pi * freq * x_fut) - coef.imag * torch.sin(2.0 * np.pi * freq * x_fut)

y_fut /= N  # Normalize the reconstructed signal

# Plot the results
plt.figure()

# Original signal
plt.subplot(2, 1, 1)
plt.plot(x.numpy(), y.numpy())
plt.title('Original Signal')
plt.xlabel('Time [s]')
plt.ylabel('Amplitude')

# Predicted future signal
plt.subplot(2, 1, 2)
plt.plot(x_fut.numpy(), y_fut.numpy())
plt.title('Predicted Future Signal')
plt.xlabel('Time [s]')
plt.ylabel('Amplitude')

plt.tight_layout()
plt.show()
