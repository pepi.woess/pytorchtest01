import torch
import matplotlib.pyplot as plt
import numpy as np

# Create a 1D time signal
N = 600  # Number of samples
T = 1.0 / 800.0  # Sample spacing
x = torch.linspace(0.0, N*T, N, dtype=torch.float64)
y = torch.sin(50.0 * 2.0 * np.pi * x) + 0.5 * torch.sin(80.0 * 2.0 * np.pi * x)

# Perform FFT
yf = torch.fft.fft(y)
xf = torch.fft.fftfreq(N, T)

# Plot the results
plt.figure()

# Original signal
plt.subplot(2, 1, 1)
plt.plot(x.numpy(), y.numpy())
plt.title('Original Signal')
plt.xlabel('Time [s]')
plt.ylabel('Amplitude')

# Frequency domain
plt.subplot(2, 1, 2)
plt.plot(xf.numpy(), 2.0/N * torch.abs(yf).numpy())
plt.title('Frequency Domain')
plt.xlabel('Frequency [Hz]')
plt.ylabel('Amplitude')

plt.tight_layout()
plt.show()
